import {gql} from 'apollo-boost'

const getBooksQuery = gql`
{
    books{
        name
        id
    }
}
`

const getAuthorsQuery = gql`
{
    authors{
        name
        id
    }
}
`

const AddBookMutation = gql`
mutation($name:String!, $genre:String!, $authorID:ID!){
    addBook(name: $name, genre: $genre, authorID: $authorID){
        name
        genre
        id
    }
}
`
const getBookQuery = gql`
query($id: ID){
    book(id: $id){
        id
        name 
        genre
        author{
            id
            name
            age 
            books{
                name
                id
            }
        }
    }
}
`

export{getBooksQuery, getAuthorsQuery, AddBookMutation, getBookQuery}