import React, { Component } from 'react';
import {graphql} from 'react-apollo' // bind apollo to react
import {flowRight as compose} from 'lodash';

import {getAuthorsQuery, AddBookMutation, getBooksQuery} from '../query/query'

class AddBook extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            genre: '',
            authorID: ''
        }
    }
    displayAuthor(){
        const data = this.props.getAuthorsQuery;
        console.log(`authos data: ${JSON.stringify(data)}`)
        if(data.loading){
            return(<option disabled>Loading Authors ...</option>)
        }else{
            return data.authors.map(author =>{
                return(<option key={author.id} value={author.id}>{author.name}</option>)
            })
        }
    }
    submitForm(e){
        e.preventDefault();
        // console.log(this.state)
        this.props.AddBookMutation({
            variables: {
                name: this.state.name,
                genre: this.state.genre,
                authorID: this.state.authorID
            },
            refetchQueries: [{query: getBooksQuery}]
        });
    }
    
    render() {
    // console.log(this.props);
    return (
       <form id="add-book" onSubmit={this.submitForm.bind(this)}>
           <div className="field">
                <label>Book Name: </label>
                <input type="text" onChange={(e)=>this.setState({name:e.target.value})}></input>
           </div>
           <div className="field">
                <label>Genre: </label>
                <input type="text" onChange={(e)=>this.setState({genre:e.target.value})}></input>
           </div>
           <div className="field">
                <label>Author: </label>
                <select onChange={(e)=>this.setState({authorID:e.target.value})}>
                    <option>Select Author</option>
                    {this.displayAuthor()}
                </select>
           </div>
           <button>+</button>
       </form>
    )
    }
}
    
export default compose(
    graphql(getAuthorsQuery, {name: "getAuthorsQuery"}),
    graphql(AddBookMutation, {name: "AddBookMutation"})
)(AddBook);
    