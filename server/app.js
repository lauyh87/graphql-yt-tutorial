const express = require('express');
const {graphqlHTTP} = require('express-graphql')
const mongoose = require('mongoose');
const cors = require('cors')
const schema = require('./schema/schema')

require('dotenv').config()
var pino = require('express-pino-logger')()


const PORT = process.env.PORT || 3000;

const app = express()
app.use(cors())
app.use(pino)
mongoose.connect("mongodb+srv://")
mongoose.connection.once('open', ()=>{
    console.log("connected to mongodb")
})
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: false
}))

app.use('/', (req, res)=>{

    res.status(200).send({'msg': 'Hello World'})
})
app.listen(PORT, ()=>{
    console.log(`Server is running on ${PORT}`)
})
